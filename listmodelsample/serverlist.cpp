
#include "serverlist.h"
#include "util.h"

#include <QVector>

template class Utils::List<Model::Server>;

namespace Model
{

ServerList::ServerList( QObject *parent )
        : ServerListInternal( parent )
{
}

ServerList::~ServerList()
{
}

int ServerList::add( QSharedPointer<Server> value )
{
    auto &servers = array;

    const auto contains = containsValue( servers, value );
    if ( contains )
    {
        return false;
    }

    if ( value->isDefault() )
    {
        // there should be only one default server
        resetDefaults();
    }

    // if it's the first server in the list, make it default
    if ( servers.size() == 0 )
    {
        value->setDefault( true );
        ServerListInternal::update();
    }

    ServerListInternal::add( value );

    return true;
}

void ServerList::remove( int index )
{
    auto &servers = array;

    bool autodefault { false };
    if ( servers.size() >= 2 )
    {
        if ( servers.at( index )->isDefault() )
        {
            resetDefaults();
            autodefault = true;
        }
    }

    ServerListInternal::remove( index );

    if ( servers.size() >= 1 && autodefault )
    {
        servers.at( 0 )->setDefault( true );
    }
}

void ServerList::resetDefaults()
{
    auto &servers = array;

    for ( const auto &item : servers )
    {
        item->setDefault( false );
        ServerListInternal::update();
    };
}

int ServerList::defaultServerIndex() const
{
    auto &servers = array;

    if ( servers.empty() )
    {
        return -1;
    }

    const auto it = std::find_if( std::begin( servers ), std::end( servers ),
        [] ( const QSharedPointer<Server> &server )
    {
        return server->isDefault();
    } );

    Q_ASSERT( it != std::end( servers ) );
    return std::distance( std::begin( servers ), it );
}

} // Model
