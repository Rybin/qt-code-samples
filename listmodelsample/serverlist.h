#ifndef SERVERLIST_H
#define SERVERLIST_H

#include "utils/list.h"
#include "server.h"

#include <QAbstractListModel>
#include <QSharedPointer>

namespace Model
{

using ServerListInternal = Utils::List<Server>;

class ServerList : public ServerListInternal
{
    Q_OBJECT

public:
    ServerList( QObject * parent = 0 );
    virtual ~ServerList();

    int add( QSharedPointer<Server> value );
    void remove( int index );

    void resetDefaults();
    int defaultServerIndex() const;
};

} // Model

#endif // SERVERLIST_H
