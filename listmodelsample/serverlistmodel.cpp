#include "serverlistmodel.h"
#include "serverlist.h"

#include <QDebug>

namespace Model
{

ServerListModel::ServerListModel( QObject *parent )
        : Utils::ListModelBase( parent )
{
}

QVariant ServerListModel::data( const QModelIndex &index, int role ) const
{
    Q_ASSERT( index.isValid() );

    if ( !index.isValid() || role != Qt::DisplayRole )
    {
        return QVariant::Invalid;
    }

    auto serverModel = dynamic_cast<ServerListInternal *>( _model.data() );
    Q_ASSERT( serverModel && "Server Model cast failure" );
    if ( nullptr == serverModel )
    {
        qDebug() << "please pass correct instance of serverModel";
        return QVariant::Invalid;
    }

    const auto row = index.row();
    if ( row < 0 || row >= serverModel->size() )
    {
        return QVariant::Invalid;
    }

    QSharedPointer<Server> serverPtr = serverModel->at( row );

    return serverPtr->toString();
}

} // Model
