#ifndef SERVERLISTMODEL_H
#define SERVERLISTMODEL_H

#include "utils/listmodelbase.h"

namespace Model
{

class ServerListModel : public Utils::ListModelBase
{
    Q_OBJECT

public:
    ServerListModel( QObject * parent = 0 );

private:
    virtual QVariant data( const QModelIndex &index, int role ) const override;
};

} // Model

#endif // SERVERLISTMODEL_H
