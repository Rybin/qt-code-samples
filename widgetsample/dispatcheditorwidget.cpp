#include "dispatcheditorwidget.h"
#include "ui_dispatcheditorwidget.h"

#include "model/dispatch.h"
#include "model/contact.h"
#include "model/attachment.h"
#include "model/attachmentlist.h"
#include "model/attachmentlistmodel.h"

#include "dispatchapplication.h"
#include "controller/mainwindowcontroller.h"
#include "controller/dispatcheditorcontroller.h"

#include <QFileDialog>
#include <QSharedPointer>
#include <QClipboard>
#include <QMessageBox>

#include <functional>

namespace View {

class DispatchEditorWidgetPrivate
{
    Q_DISABLE_COPY( DispatchEditorWidgetPrivate )

public:
    DispatchEditorWidgetPrivate( DispatchEditorWidget *q_ptr )
        : q_ptr( q_ptr )
    {
        Q_ASSERT( q_ptr );
    }

    ~DispatchEditorWidgetPrivate() = default;

    void setModel( QSharedPointer<Model::Dispatch> model )
    {
        dispatch = model;
        fillUiFromModel();
        connectModel();
    }

    void fillUiFromModel()
    {
        Q_ASSERT( !dispatch.isNull() );

        // subject
        setSubject( dispatch->subject() );

        // to, cc, bcc
        fillContacts();

        // attachments
        auto dispatchWidget = ui->dispatchBodyWidget;
        attachmentListModel.reset( new Model::AttachmentListModel( dispatch->attachments() ) );

        dispatchWidget->setAttachmentListModel( &*attachmentListModel );

        // body
        dispatchWidget->setDispatchModel( dispatch.data() );
    }

    void fillContacts()
    {
        const auto &to = dispatch->recipients().to();
        const auto &cc = dispatch->recipients().copy();
        const auto &bcc = dispatch->recipients().hiddenCopy();

        setTo( contactsToString( to ) );
        setCc( contactsToString( cc ) );
        setBcc( contactsToString( bcc ) );
    }

    void connectModel()
    {
        QObject::connect( &dispatch->recipients(), &Model::RecipientList::changed,
            std::bind( &DispatchEditorWidgetPrivate::fillContacts, this ) );
    }

    void writeToModel()
    {
        Q_ASSERT( !dispatch.isNull() );

        // write subject
        dispatch->setSubject( ui->subjectLineEdit->text() );

        // write body
        ui->dispatchBodyWidget->writeToModel();

        // parse contacts
        auto parseContacts = [this]( const QString &text, Model::RecipientType type )
        {
            const auto entries = text.split( "," );
            for ( const auto &entry : entries )
            {
                // FIXME: adds duplicates, incorrectly parses entries with name
                auto contact = QSharedPointer<Model::Contact>( new Model::Contact( entry ) );
                dispatch->recipients().addRecipient( contact, type );
            }
        };

        parseContacts( ui->toLineEdit->text(), Model::RecipientType::To );
        parseContacts( ui->ccLineEdit->text(), Model::RecipientType::Copy );
        parseContacts( ui->bccLineEdit->text(), Model::RecipientType::HiddenCopy );
    }

    void setTo( const QString &to )
    {
        ui->toLineEdit->setText( to );
    }

    void setCc( const QString &cc )
    {
        ui->ccLineEdit->setText( cc );
    }

    void setBcc( const QString &bcc )
    {
        ui->bccLineEdit->setText( bcc );
    }

    void setSubject( const QString &subject )
    {
        ui->subjectLineEdit->setText( subject );
    }

    void save()
    {
        writeToModel();
        emit q_ptr->saveRequested( dispatch );
    }


    const QScopedPointer<Ui::DispatchEditorWidget> ui { new Ui::DispatchEditorWidget };
    DispatchEditorWidget *q_ptr { nullptr };
    QSharedPointer<Model::Dispatch> dispatch;
    QScopedPointer<Model::AttachmentListModel> attachmentListModel;
};

DispatchEditorWidget::DispatchEditorWidget( QWidget *parent )
        : QWidget( parent )
        , d_ptr( new DispatchEditorWidgetPrivate( this ) )
{
    Q_D( DispatchEditorWidget );
    d->ui->setupUi( this );

    d->ui->dispatchBodyWidget->setEditable( true );

    d->ui->sendButton->setEnabled( false );
    QObject::connect( d->ui->toLineEdit, &QLineEdit::textChanged,
        [d]( const QString &text )
    {
        d->ui->sendButton->setEnabled( !text.isEmpty() );
    } );

    setModel( QSharedPointer<Model::Dispatch>( new Model::Dispatch ) );
}

DispatchEditorWidget::~DispatchEditorWidget()
{
}

void DispatchEditorWidget::setModel( QSharedPointer<Model::Dispatch> dispatch )
{
    Q_D( DispatchEditorWidget );

    // disconnect from old model
    if ( !d->dispatch.isNull() )
    {
        d->dispatch->disconnect( this );
    }

    d->setModel( dispatch );
}

void DispatchEditorWidget::on_toButton_clicked()
{
    Q_D( DispatchEditorWidget );
    DispatchApp->dispatchEditorController()->showSelectRecipientsForm( d->dispatch );
}

void DispatchEditorWidget::on_ccButton_clicked()
{
    Q_D( DispatchEditorWidget );
    DispatchApp->dispatchEditorController()->showSelectRecipientsForm( d->dispatch );
}

void DispatchEditorWidget::on_bccButton_clicked()
{
    Q_D( DispatchEditorWidget );
    DispatchApp->dispatchEditorController()->showSelectRecipientsForm( d->dispatch );
}

void DispatchEditorWidget::on_copyButton_clicked()
{
    Q_D( DispatchEditorWidget );
    auto clipboard = QApplication::clipboard();
    auto text = d->ui->dispatchBodyWidget->selectedText();
    if ( text.isEmpty() )
    {
        return;
    }

    clipboard->setText( text );
}

void DispatchEditorWidget::on_pasteButton_clicked()
{
    Q_D( DispatchEditorWidget );
    auto clipboard = QApplication::clipboard();
    d->ui->dispatchBodyWidget->paste( clipboard->text() );
}

void DispatchEditorWidget::on_attachButton_clicked()
{
    Q_D( DispatchEditorWidget );
    auto filePath = QFileDialog::getOpenFileName( this, QObject::tr( "Attach File" ) );

    // dialog was cancelled
    if ( filePath.isEmpty() )
    {
        return;
    }

    auto newAttachment = Model::Attachment::create( filePath );

    Q_ASSERT( !newAttachment.isNull() );
    if ( newAttachment.isNull() )
    {
        return;
    }

    auto attachments = d->dispatch->attachments();
    attachments->addAttachment( newAttachment );
}

void DispatchEditorWidget::on_cancelButton_clicked()
{
    Q_D( DispatchEditorWidget );
    auto ret = QMessageBox( QObject::tr( "Cancel Compose?" ),
         QObject::tr( "You are about to cancel a compose. Would you like to save your changes?" ),
         QMessageBox::Question,
         QMessageBox::Yes, QMessageBox::No, QMessageBox::Cancel ).exec();

    if ( ret == QMessageBox::Cancel )
    {
        return;
    }
    else if ( ret == QMessageBox::Yes )
    {
        d->save();
    }

    DispatchApp->mainWindowController()->setLoggedIn( true );
}

void DispatchEditorWidget::on_sendButton_clicked()
{
    Q_D( DispatchEditorWidget );
    d->writeToModel();
    emit sendRequested( d->dispatch );
}

void DispatchEditorWidget::on_saveButton_clicked()
{
    Q_D( DispatchEditorWidget );
    d->save();
}

} // namespace View
