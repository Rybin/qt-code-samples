#ifndef DISPATCHEDITORWIDGET_H
#define DISPATCHEDITORWIDGET_H

#include <QWidget>
#include <QSharedPointer>

namespace Model {
class Dispatch;
enum class RecipientType;
}

namespace View {

class DispatchEditorWidgetPrivate;
class DispatchEditorWidget : public QWidget
{
    Q_OBJECT

public:
    explicit DispatchEditorWidget( QWidget *parent = 0 );
    ~DispatchEditorWidget();

    void setModel( QSharedPointer<Model::Dispatch> dispatch );

signals:
    void saveRequested( QSharedPointer<Model::Dispatch> dispatch );
    void sendRequested( QSharedPointer<Model::Dispatch> dispatch );
    void addContactFromList( Model::RecipientType type );

protected:
    Q_DECLARE_PRIVATE( DispatchEditorWidget )
    const QScopedPointer< DispatchEditorWidgetPrivate > d_ptr;

private slots:
    void on_sendButton_clicked();
    void on_saveButton_clicked();
    void on_cancelButton_clicked();
    void on_copyButton_clicked();
    void on_pasteButton_clicked();
    void on_attachButton_clicked();
    void on_toButton_clicked();
    void on_ccButton_clicked();
    void on_bccButton_clicked();
};

} // namespace View

#endif // DISPATCHEDITORWIDGET_H
